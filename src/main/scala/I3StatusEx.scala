import scala.sys.process.Process
import scala.util.matching.Regex.Match

object I3StatusEx {

  /**
    * Opens the i3status stream and processes each line at a time.
    *
    * @param args Standard 'main' command line arguments.
    */
  def main(args: Array[String]) = {
    val process = Process("i3status -c ~/.config/i3/i3status.conf").lineStream
    process.foreach(parseAndPrint)
  }

  /**
    * Parses the given line searching, using regex, to find the battery percentage and replace it with a bar of blocks.
    *
    * @param in The input line from i3status to parse.
    */
  def parseAndPrint(in: String) = {
    val pattern = """\[\d+\.\d+%\]""".r
    val result: Option[Match] = pattern.findFirstMatchIn(in)
    if (!result.isEmpty) {
      val bar = percentToBars(result.get.toString().substring(1,6).toFloat)
      println(pattern.replaceFirstIn(in, bar))
    } else {
      println(in)
    }
  }

  /**
    * Converts the given i3status battery percentage to a string of blocks.
    *
    * @param percent The percentage of the string that will be filled with bars.
    * @return A string in the form [   ########]
    */
  def percentToBars(percent: Float) = {
    val topline = 72.00
    val baseline = 30
    val sectionCount = 15

    val outOf1 = (percent - baseline) / (topline - baseline)
    val fullBarCount = Math.round(outOf1 * sectionCount).toInt
    val emptyBarCount = sectionCount - fullBarCount

    val fullString = "#" * fullBarCount
    val emptyString = " " * emptyBarCount

    "[" + emptyString  + fullString + "|" + percent.toInt + "%]"
  }

}
